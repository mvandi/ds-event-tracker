@echo off

set ALARM_FLAG="-a"
set BUTTON_FLAG="-b"
set LIGHT_FLAG="-l"
set EMU_FLAG="-e"

set argc=0
FOR %%x in (%*) DO (
    set /A argc+=1
    set "argv[!argc!]=%%~x"
)

IF /I "%argc%" == "1" (
    java -cp build\libs\event-tracker-1.0-SNAPSHOT.jar it.unibo.ds.event_tracker.EventTrackerServer %1
) ELSE (
    goto usage
)
goto exit_program

:usage
@echo "usage: %0 <port>"

:exit_program
