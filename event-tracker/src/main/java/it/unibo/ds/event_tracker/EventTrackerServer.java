package it.unibo.ds.event_tracker;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.reactivex.core.eventbus.Message;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.handler.StaticHandler;
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.net.InetAddress;

import static it.unibo.ds.messages.Channels.EVENTS;
import static it.unibo.ds.messages.Channels.WEB_EVENTS;

public class EventTrackerServer extends RxVerticleBase {

    private Integer port;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        port = config().getInteger("port");
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isCommunication)
                .subscribe(this::onMessage);

        final Router router = Router.router(vertx);
        router.route("/event-tracker/*").handler(sockJSHandler());
        router.route().handler(StaticHandler.create());
        final String address = InetAddress.getLocalHost().getHostAddress();
        vertx.createHttpServer().requestHandler(router::accept).listen(port, ar -> {
            if (ar.succeeded()) {
                logger.info("Server started, started on http://{0}:{1,number,#}", address, ar.result().actualPort());
            } else {
                logger.error("Could not start the server: {0}", ar.cause().getMessage());
            }
        });
    }

    private void onMessage(final JsonObject message) {
        eventBus.publish(WEB_EVENTS, message);
        logger.info("Propagated message to web clients");
    }

    private SockJSHandler sockJSHandler() {
        return SockJSHandler.create(vertx).bridge(new BridgeOptions()
                .addOutboundPermitted(new PermittedOptions().setAddress(WEB_EVENTS)));
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("port", Integer.parseInt(args[0]))))
                .deploy(EventTrackerServer::new);
    }

}
