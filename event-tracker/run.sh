#!/bin/bash
usage() {
    echo -e "usage: $0 <port>"
}

if [ "$#" -ne "1" ]; then
    usage
    exit 1
fi
java -cp build/libs/event-tracker-1.0-SNAPSHOT.jar it.unibo.ds.event_tracker.EventTrackerServer $1
