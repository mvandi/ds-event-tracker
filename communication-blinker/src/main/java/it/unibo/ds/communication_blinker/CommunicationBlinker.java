package it.unibo.ds.communication_blinker;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import it.unibo.ds.devices.Light;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.util.function.IntFunction;

import static it.unibo.ds.messages.Channels.EVENTS;

public class CommunicationBlinker extends RxVerticleBase {

    private Light communicationLight;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        final boolean emulated = config().getBoolean("emulated");
        final int pin = config().getInteger("pin");
        communicationLight = getLightFactory(emulated).apply(pin);
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isCommunication)
                .subscribe(this::onCommunication);

        logger.info("Started");
    }

    private void onCommunication(final JsonObject message) {
        communicationLight.switchOn();
        logger.info("Communication LED switched on");
        // After 200 ms switch off the communication light
        vertx.setTimer(200, this::onTimeout);
        logger.info("Started timer");
    }

    private void onTimeout(final long tid) {
        communicationLight.switchOff();
        logger.info("Communication LED switched off");
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("emulated", Boolean.parseBoolean(args[0]))
                        .put("pin", Integer.parseInt(args[1]))))
                .deploy(CommunicationBlinker::new);
    }

    private static IntFunction<? extends Light> getLightFactory(final boolean emulated) {
        return emulated
                ?  it.unibo.ds.devices.emu.Led::new
                :  it.unibo.ds.devices.pi4j.Led::new;
    }

}
