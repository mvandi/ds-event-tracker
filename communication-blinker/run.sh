#!/bin/bash
EMU_FLAG=-e

usage() {
    echo -e "usage: $0 [$EMU_FLAG] <pin>"
    echo -e "\t$EMU_FLAG start in emulated mode"
}

if [ "$#" -lt "1" ]; then
    usage
    exit 1
fi
if [[ "$1" -ne "$EMU_FLAG" ]]; then
    emu="false"
    pin=$1
else
    if [ "$#" -ne "2" ]; then
        usage
        exit 1
    fi
    emu="true"
    pin=$2
fi
java -cp build/libs/communication-blinker-1.0-SNAPSHOT.jar it.unibo.ds.communication_blinker.CommunicationBlinker $emu $pin
