@echo off

set EMU_FLAG=-e

set argc=0
FOR %%x in (%*) DO (
    set /A argc+=1
    set "argv[!argc!]=%%~x"
)

IF /I "%argc%" LSS "1" (
    goto usage
)
IF /I "%1" == "%EMU_FLAG%" (
    IF /I "%argc%" == "2" (
        set emu="true"
        set pin=%2
    ) ELSE (
        goto usage
    )
) ELSE (
    set emu="false"
    set pin=%1
)
java -cp build\libs\communication-blinker-1.0-SNAPSHOT.jar it.unibo.ds.communication_blinker.CommunicationBlinker %emu% %pin%

goto exit_program

:usage
@echo "usage: %0 [%EMU_FLAG%] <pin>"
@echo " %EMU_FLAG% start in emulated mode"

:exit_program
