package it.unibo.ds.vertx;

import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.EventBus;

public class RxVerticleBase extends AbstractVerticle {

    protected EventBus eventBus;
    protected Logger logger;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        eventBus = this.vertx.eventBus();
        logger = LoggerFactory.getLogger(getClass());
    }

}
