package it.unibo.ds.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class VerticleBase extends AbstractVerticle {

    protected EventBus eventBus;
    protected Logger logger;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        eventBus = this.vertx.eventBus();
        logger = LoggerFactory.getLogger(getClass());
    }

}
