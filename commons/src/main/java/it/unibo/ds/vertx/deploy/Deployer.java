package it.unibo.ds.vertx.deploy;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.Verticle;
import io.vertx.core.VertxOptions;

import java.util.Objects;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * @author Mattia Vandi
 */
public interface Deployer {

    /**
     * Creates a deployer instance.
     *
     * @return the deployer instance
     */
    static Deployer deployer() {
        return new DeployerImpl();
    }

    /**
     * Creates a deployer instance.
     *
     * @param configurator the deployer configurator
     * @return the deployer instance
     */
    static Deployer deployer(final Handler<? super Deployer> configurator) {
        requireNonNull(configurator);
        final Deployer deployer = deployer();

        configurator.handle(deployer);

        return deployer;
    }

    /**
     * Deploys a {@link io.vertx.core.Verticle} using the execute around pattern.
     *
     * @param verticleSupplier the supplier of the verticle to be deployed
     * @param configurator     the deployer configurator
     */
    static void deploy(final Supplier<? extends Verticle> verticleSupplier, final Handler<? super Deployer> configurator) {
        deployer(configurator).deploy(verticleSupplier);
    }

    /**
     * Deploys a {@link io.vertx.core.Verticle} using the execute around pattern.
     *
     * @param verticle     the verticle to be deployed
     * @param configurator the deployer configurator
     */
    static void deploy(final Verticle verticle, final Handler<? super Deployer> configurator) {
        deployer(configurator).deploy(verticle);
    }

    /**
     * Deploys a {@link io.vertx.core.Verticle} using the execute around pattern.
     *
     * @param verticleClass the class of the verticle to be deployed
     * @param configurator  the deployer configurator
     */
    static void deploy(final Class<? extends Verticle> verticleClass, final Handler<? super Deployer> configurator) {
        deployer(configurator).deploy(verticleClass);
    }

    /**
     * Sets whether or not the {@link io.vertx.core.Vertx} instance will be clustered.
     *
     * @param clustered if true, the Vert.x instance will be clustered, otherwise not
     * @return a reference to this, so the API can be used fluently
     */
    Deployer clustered(boolean clustered);

    /**
     * Sets the Vert.x configuration to configure the {@link io.vertx.core.Vertx} instance.
     *
     * @param vertxOptions the Vert.x options
     * @return a reference to this, so the API can be used fluently
     */
    Deployer vertxOptions(VertxOptions vertxOptions);

    /**
     * Sets the options for configuring the {@link io.vertx.core.Verticle} deployment.
     *
     * @param deploymentOptions the deployment options
     * @return a reference to this, so the API can be used fluently
     */
    Deployer deploymentOptions(DeploymentOptions deploymentOptions);

    /**
     * Sets the handler to be called when the {@link io.vertx.core.Verticle} is deployed.
     *
     * @param completionHandler the deployment completion handler
     * @return a reference to this, so the API can be used fluently
     */
    Deployer handler(Handler<? super String> completionHandler);

    /**
     * Sets the handler to be called in case of a failure during the {@link io.vertx.core.Verticle} deployment.
     *
     * @param failureHandler the failure handler
     * @return a reference to this, so the API can be used fluently
     */
    Deployer failureHandler(Handler<? super Throwable> failureHandler);

    /**
     * Deploys the specified {@link io.vertx.core.Verticle}.
     *
     * @param verticleClass the class of the verticle to be deployed
     */
    void deploy(Class<? extends Verticle> verticleClass);

    /**
     * Deploys the specified {@link io.vertx.core.Verticle}.
     *
     * @param verticleSupplier the supplier of the verticle to be deployed
     */
    void deploy(Supplier<? extends Verticle> verticleSupplier);

    /**
     * Deploys the specified {@link io.vertx.core.Verticle}.
     *
     * @param verticle the verticle to be deployed
     */
    default void deploy(final Verticle verticle) {
        Objects.requireNonNull(verticle, "verticle is null");
        deploy(() -> verticle);
    }

}