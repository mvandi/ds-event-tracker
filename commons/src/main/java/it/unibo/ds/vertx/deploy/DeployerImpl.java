package it.unibo.ds.vertx.deploy;

import io.vertx.core.*;

import java.net.InetAddress;
import java.util.Objects;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * @author Mattia Vandi
 */
class DeployerImpl implements Deployer {

    private boolean clustered;
    private VertxOptions vertxOptions;
    private DeploymentOptions deploymentOptions;

    private Handler<? super String> completionHandler;
    private Handler<? super Throwable> failureHandler;

    DeployerImpl() {
    }

    @Override
    public DeployerImpl clustered(final boolean clustered) {
        this.clustered = clustered;
        return this;
    }

    @Override
    public DeployerImpl vertxOptions(final VertxOptions vertxOptions) {
        this.vertxOptions = vertxOptions;
        return this;
    }

    @Override
    public DeployerImpl deploymentOptions(final DeploymentOptions deploymentOptions) {
        this.deploymentOptions = deploymentOptions;
        return this;
    }

    @Override
    public DeployerImpl handler(final Handler<? super String> completionHandler) {
        this.completionHandler = completionHandler;
        return this;
    }

    @Override
    public DeployerImpl failureHandler(final Handler<? super Throwable> failureHandler) {
        this.failureHandler = failureHandler;
        return this;
    }

    @Override
    public void deploy(final Class<? extends Verticle> verticleClass) {
        requireNonNull(verticleClass);
        deploy(() -> {
            try {
                return verticleClass.newInstance();
            } catch (final Throwable cause) {
                handleFailure(cause);
                return null;
            }
        });
    }

    @Override
    public void deploy(final Supplier<? extends Verticle> verticleSupplier) {
        requireNonNull(verticleSupplier);

        final VertxOptions options = vertxOptions();

        if (clustered || options.isClustered()) {
            options.setClustered(true);
            final String clusterHost = options.getClusterHost();
            if (clusterHost == null || Objects.equals(clusterHost, VertxOptions.DEFAULT_CLUSTER_HOST)) {
                try {
                    options.setClusterHost(InetAddress.getLocalHost().getHostAddress());
                } catch (final Throwable cause) {
                    handleFailure(cause);
                    return;
                }
            }
        }

        if (options.isClustered()) {
            Vertx.clusteredVertx(options, ar -> {
                if (ar.succeeded()) {
                    deployVerticle(ar.result(), verticleSupplier);
                } else {
                    handleFailure(ar.cause());
                }
            });
        } else {
            deployVerticle(Vertx.vertx(options), verticleSupplier);
        }
    }

    private void deployVerticle(final Vertx vertx, final Supplier<? extends Verticle> verticleSupplier) {
        final Verticle verticle = Utils.getNonNull(verticleSupplier, "verticle is null");
        vertx.deployVerticle(verticle, deploymentOptions(), ar -> {
            if (ar.succeeded()) {
                handle(ar.result());
            } else {
                handleFailure(ar.cause());
            }
        });
    }

    private VertxOptions vertxOptions() {
        return Utils.getNonNull(vertxOptions, VertxOptions::new);
    }

    private DeploymentOptions deploymentOptions() {
        return Utils.getNonNull(deploymentOptions, DeploymentOptions::new);
    }

    private void handle(final String deploymentID) {
        handle(completionHandler, deploymentID);
    }

    private void handleFailure(final Throwable cause) {
        handle(failureHandler, cause);
    }

    private static <T> void handle(final Handler<? super T> handler, final T t) {
        Utils.handler(handler).handle(t);
    }

}