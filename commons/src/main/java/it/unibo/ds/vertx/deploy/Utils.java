package it.unibo.ds.vertx.deploy;

import io.vertx.core.Handler;

import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * @author Mattia Vandi
 */
final class Utils {

    static <T> Handler<T> handler(Handler<? super T> handler) {
        return t -> {
            if (handler != null) {
                handler.handle(t);
            }
        };
    }

    static <T> T require(T t, Predicate<? super T> predicate) {
        return require(t, EMPTY_STR, predicate);
    }

    private static <T> T require(T t, String message, Predicate<? super T> predicate) {
        requireNonNull(predicate, "predicate is null");
        if (!predicate.test(t)) {
            throw newIllegalArgumentException(message);
        }
        return t;
    }

    static <T> T getNonNull(Supplier<? extends T> supplier) {
        return getNonNull(supplier, EMPTY_STR);
    }

    static <T> T getNonNull(Supplier<? extends T> supplier, String message) {
        return get(supplier, message, Objects::nonNull);
    }

    private static <T> T get(Supplier<? extends T> supplier, Predicate<? super T> predicate) {
        return get(supplier, EMPTY_STR, predicate);
    }

    private static <T> T get(Supplier<? extends T> supplier, String message, Predicate<? super T> predicate) {
        requireNonNull(supplier, "supplier is null");
        return require(supplier.get(), message, predicate);
    }

    static <T> T getNonNull(T value, T defaultValue) {
        return getNonNull(value, () -> defaultValue);
    }

    static <T> T getNonNull(T value, Supplier<? extends T> defaultSupplier) {
        return getOrDefault(value, defaultSupplier, Objects::nonNull);
    }

    private static <T> T getOrDefault(T value, Supplier<? extends T> defaultSupplier, Predicate<? super T> predicate) {
        requireNonNull(predicate, "predicate is null");
        return !predicate.test(value) ? get(defaultSupplier, predicate) : value;
    }

    private static IllegalArgumentException newIllegalArgumentException(String message) {
        return message == null || message.isEmpty()
                ? new IllegalArgumentException()
                : new IllegalArgumentException(message);
    }

    private static final String EMPTY_STR = "";

    private Utils() {
        throw new IllegalAccessError(String.format("No instances for %s!", getClass().getName()));
    }

}