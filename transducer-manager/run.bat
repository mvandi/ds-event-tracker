@echo off

set ACTUATOR_FLAG=-a
set PRESENCE_FLAG=-p
set TEMPERATURE_FLAG=-t
set EMU_FLAG=-e

set argc=0
FOR %%x in (%*) DO (
    set /A argc+=1
    set "argv[!argc!]=%%~x"
)

IF /I "%argc%" LSS "1" (
    goto usage
)
set PACKAGE=it.unibo.ds.transducer_manager
IF /I "%1" == "%ACTUATOR_FLAG%" (
    set mainClass=%PACKAGE%.ActuatorManager
) ELSE if /I "%1" == "%PRESENCE_FLAG%" (
    set mainClass=%PACKAGE%.PresenceManager
) ELSE if /I "%1" == "%TEMPERATURE_FLAG%" (
    set mainClass=%PACKAGE%.TemperatureManager
) ELSE (
    echo "Wrong flag %1" >&2
    goto usage
)
set OUTPUT_DIR=build\libs
if /I "%argc%" LSS "2" (
    goto usage
)
IF /I "%2" == "%EMU_FLAG%" (
    if /I "%argc%" == "3" (
        set emu="true"
        set pin=%3
    ) ELSE (
        goto usage
    )
) ELSE (
    IF /I "%argc%" == "2" (
        set emu="false"
        set pin=%2
    ) ELSE (
        goto usage
    )
)
java -cp %OUTPUT_DIR%/transducer-manager-1.0-SNAPSHOT.jar %mainClass% %emu% %pin%
goto exit_program

:usage
@echo "usage: %0 <%ACTUATOR_FLAG%|%PRESENCE_FLAG%|%TEMPERATURE_FLAG%> [%EMU_FLAG%] <pin>"
@echo " %ACTUATOR_FLAG% start the actuator verticle"
@echo " %PRESENCE_FLAG% start the presence verticle"
@echo " %TEMPERATURE_FLAG% start the temperature verticle"
@echo " %EMU_FLAG% start in emulated mode"

:exit_program
