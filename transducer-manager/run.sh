#!/bin/bash
ACTUATOR_FLAG=-a
PRESENCE_FLAG=-p
TEMPERATURE_FLAG=-t
EMU_FLAG=-e

usage() {
    echo -e "usage: $0 <$ACTUATOR_FLAG|$PRESENCE_FLAG|$TEMPERATURE_FLAG> [$EMU_FLAG] <pin>"
    echo -e "\t$ACTUATOR_FLAG start the actuator verticle"
    echo -e "\t$PRESENCE_FLAG start the presence detector verticle"
    echo -e "\t$TEMPERATURE_FLAG start the temperature sensor verticle"
    echo -e "\t$EMU_FLAG start in emulated mode"
}

if [ "$#" -lt "1" ]; then
    usage
    exit 1
fi

PACKAGE=it.unibo.ds.transducer_manager
if [ "$1" == "$ACTUATOR_FLAG" ]; then
    mainClass=$PACKAGE.ActuatorManager
elif [ "$1" == "$PRESENCE_FLAG" ]; then
    mainClass=$PACKAGE.PresenceManager
elif [ "$1" == "$TEMPERATURE_FLAG" ]; then
    mainClass=$PACKAGE.TemperatureManager
else
    usage
    exit 1
fi
OUTPUT_DIR=build/libs
if [ "$#" -lt "2" ]; then
    usage
    exit 1
fi
if [[ "$2" -ne "$EMU_FLAG" ]]; then
    emu="false"
    pin=$2
else
    if [ "$#" -ne "3" ]; then
        usage
        exit 1
    fi
    emu="true"
    pin=$3
fi
java -cp $OUTPUT_DIR/transducer-manager-1.0-SNAPSHOT.jar $mainClass $emu $pin
