package it.unibo.ds.transducer_manager;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import it.unibo.ds.devices.TemperatureSensor;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.messages.Temperature;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntFunction;

import static it.unibo.ds.messages.Channels.EVENTS;

public class TemperatureManager extends RxVerticleBase {

    private enum State {
        NO_ALARM,
        ALARM
    }

    private final AtomicReference<State> state = new AtomicReference<>(State.NO_ALARM);
    private TemperatureSensor temperatureSensor;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        final boolean emulated = config().getBoolean("emulated");
        final int pin = config().getInteger("pin");
        temperatureSensor = getTemperatureSensorFactory(emulated).apply(pin);
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isStartAlarm)
                .subscribe(this::onStartAlarm);
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isStopAlarm)
                .subscribe(this::onStopAlarm);
        vertx.setPeriodic(1_000, this::onTimeout);

        logger.info("Started");
    }

    private void onStartAlarm(final JsonObject message) {
        alarm();
        logger.info("Alarm started");
    }

    private void onStopAlarm(final JsonObject message) {
        noAlarm();
        logger.info("Alarm stopped");
    }

    private void onTimeout(final long tid) {
        if (isNoAlarm()) {
            final Temperature temperature = new Temperature(temperatureSensor.getTemperature());
            logger.info("Sending temperature of {0}°C", temperature.getValue());
            eventBus.publish(EVENTS, MessageConverter.toJson(temperature));
        }
    }

    private void alarm() {
        state.set(State.ALARM);
    }

    private void noAlarm() {
        state.set(State.NO_ALARM);
    }

    private boolean isNoAlarm() {
        return state.get() == State.NO_ALARM;
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("emulated", Boolean.parseBoolean(args[0]))
                        .put("pin", Integer.parseInt(args[1]))))
                .deploy(TemperatureManager::new);
    }

    private static IntFunction<? extends TemperatureSensor> getTemperatureSensorFactory(final boolean emulated) {
        return emulated
                ?  it.unibo.ds.devices.emu.TemperatureSensor::new
                :  it.unibo.ds.devices.pi4j.DHT11::new;
}

}
