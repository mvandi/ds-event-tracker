package it.unibo.ds.transducer_manager;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import it.unibo.ds.devices.MotionDetector;
import it.unibo.ds.devices.emu.Pir;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.messages.Presence;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntFunction;

import static it.unibo.ds.messages.Channels.EVENTS;

public class PresenceManager extends RxVerticleBase {

    private enum State {
        DETECTING,
        DETECTED,
        ALARM
    }

    private final AtomicReference<State> state = new AtomicReference<>(State.DETECTING);
    private MotionDetector motionDetector;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        final boolean emulated = config().getBoolean("emulated");
        final int pin = config().getInteger("pin");
        motionDetector = getMotionDetectorFactory(emulated).apply(pin);
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isStartAlarm)
                .subscribe(this::onStartAlarm);
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(event -> MessageConverter.isIgnoreAlarm(event) || MessageConverter.isStopAlarm(event))
                .subscribe(this::onResponse);
        vertx.setPeriodic(200, this::onPresence);

        logger.info("Started");
    }

    private void onResponse(final JsonObject message) {
        detecting();
        logger.info("Alarm either ignored or stopped");
    }

    private void onStartAlarm(final JsonObject message) {
        alarm();
        logger.info("Alarm started");
    }

    private void onPresence(final long tid) {
        if (isDetecting() && motionDetector.detected()) {
            detected();
            logger.info("Detected presence");
            eventBus.publish(EVENTS, MessageConverter.toJson(new Presence()));
        }
    }

    private boolean isDetecting() {
        return state.get() == State.DETECTING;
    }

    private void detecting() {
        state.set(State.DETECTING);
    }

    private void detected() {
        state.set(State.DETECTED);
    }

    private void alarm() {
        state.set(State.ALARM);
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("emulated", Boolean.parseBoolean(args[0]))
                        .put("pin", Integer.parseInt(args[1]))))
                .deploy(PresenceManager::new);
    }

    private static IntFunction<? extends MotionDetector> getMotionDetectorFactory(final boolean emulated) {
        return emulated
                ?  it.unibo.ds.devices.emu.Pir::new
                :  it.unibo.ds.devices.pi4j.Pir::new;
    }

}
