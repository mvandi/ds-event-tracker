package it.unibo.ds.transducer_manager;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import it.unibo.ds.devices.Actuator;
import it.unibo.ds.devices.Light;
import it.unibo.ds.devices.emu.Servo;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.util.function.IntFunction;

import static it.unibo.ds.messages.Channels.EVENTS;

public class ActuatorManager extends RxVerticleBase {

    private Actuator actuator;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        final boolean emulated = config().getBoolean("emulated");
        final int pin = config().getInteger("pin");
        actuator = getActuatorFactory(emulated).apply(pin);
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isStartAlarm)
                .subscribe(this::onStartAlarm);
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isStopAlarm)
                .subscribe(this::onStopAlarm);

        logger.info("Started");
    }

    private void onStartAlarm(final JsonObject message) {
        this.actuator.startMovement();
        logger.info("Actuator started");
    }

    private void onStopAlarm(final JsonObject message) {
        this.actuator.stopMovement();
        logger.info("Actuator stopped");
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("emulated", Boolean.parseBoolean(args[0]))
                        .put("pin", Integer.parseInt(args[1]))))
                .deploy(ActuatorManager::new);
    }

    private static IntFunction<? extends Actuator> getActuatorFactory(final boolean emulated) {
        return i -> new Servo();
    }

}
