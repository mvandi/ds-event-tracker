package it.unibo.ds.events;

public abstract class Event<S extends EventSource> {

    private final S source;

    protected Event(final S source) {
        this.source = source;
    }

    public final S getSource() {
        return source;
    }

}
