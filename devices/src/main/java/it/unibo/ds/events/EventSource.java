package it.unibo.ds.events;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public abstract class EventSource<E extends Event<?>> {

    private final PublishSubject<E> subject;

    protected EventSource() {
        subject = PublishSubject.create();
    }

    public final Observable<E> toObservable() {
        return subject;
    }

    protected final void notifyEvent(final E event) {
        subject.onNext(event);
    }

}
