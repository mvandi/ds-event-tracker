package it.unibo.ds.devices;

public interface TemperatureSensor {
    double getTemperature();
}
