package it.unibo.ds.devices;

import it.unibo.ds.events.Event;

public class Tick extends Event<ObservableTimer> {

    private final long time;

    public Tick(ObservableTimer source, long time) {
        super(source);
        this.time = time;
    }

    public long getTime() {
        return time;
    }
}
