package it.unibo.ds.devices.pi4j;

import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import it.unibo.ds.devices.ButtonPressed;
import it.unibo.ds.devices.ButtonReleased;
import it.unibo.ds.devices.ObservableButton;
import it.unibo.ds.events.Event;

public class Button extends ObservableButton {

    private GpioPinDigitalInput pin;

    public Button(int pinNum) {
        GpioController gpio = GpioFactory.getInstance();
        pin = gpio.provisionDigitalInputPin(Config.getPin(pinNum), PinPullResistance.PULL_DOWN);

        pin.addListener(new ButtonListener(this));
    }

    @Override
    public synchronized boolean isPressed() {
        return pin.isHigh();
    }

    private class ButtonListener implements GpioPinListenerDigital {
        private final Button button;

        ButtonListener(Button button) {
            this.button = button;
        }

        @Override
        public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            Event<ObservableButton> ev = null;
            if (event.getState().isHigh()) {
                ev = new ButtonPressed(button);
            } else {
                ev = new ButtonReleased(button);
            }
            notifyEvent(ev);
        }
    }

}
