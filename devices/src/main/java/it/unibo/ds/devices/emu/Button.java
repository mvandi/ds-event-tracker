package it.unibo.ds.devices.emu;

import it.unibo.ds.devices.ButtonPressed;
import it.unibo.ds.devices.ButtonReleased;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Button extends it.unibo.ds.devices.ObservableButton {

    private boolean pressed;

    public Button(int pinNum) {
        try {
            ButtonFrame buttonFrame = new ButtonFrame(pinNum);
            buttonFrame.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public synchronized boolean isPressed() {
        return pressed;
    }

    void setPressed(boolean state) {
        pressed = state;
        if (pressed) {
            this.notifyEvent(new ButtonPressed(this));
        } else {
            this.notifyEvent(new ButtonReleased(this));
        }
    }

    class ButtonFrame extends JFrame implements MouseListener {
        public ButtonFrame(int pin) {
            super("Button Emu");
            setSize(200, 50);
            JButton button = new JButton("Button on pin: " + pin);
            button.addMouseListener(this);
            getContentPane().add(button);
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent ev) {
                    System.exit(-1);
                }
            });
        }

        @Override
        public void mousePressed(MouseEvent e) {
            setPressed(true);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            setPressed(false);
        }

        @Override
        public void mouseEntered(MouseEvent e) {
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }
    }

}
