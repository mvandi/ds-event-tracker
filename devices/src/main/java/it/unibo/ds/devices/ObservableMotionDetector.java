package it.unibo.ds.devices;

import it.unibo.ds.events.Event;
import it.unibo.ds.events.EventSource;

public abstract class ObservableMotionDetector extends EventSource<Event<ObservableMotionDetector>> implements MotionDetector {
}
