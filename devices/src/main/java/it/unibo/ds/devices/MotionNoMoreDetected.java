package it.unibo.ds.devices;


import it.unibo.ds.events.Event;

public class MotionNoMoreDetected extends Event<ObservableMotionDetector> {

    public MotionNoMoreDetected(ObservableMotionDetector source) {
        super(source);
    }

}
