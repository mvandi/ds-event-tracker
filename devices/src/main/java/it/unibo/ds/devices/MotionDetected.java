package it.unibo.ds.devices;


import it.unibo.ds.events.Event;

public class MotionDetected extends Event<ObservableMotionDetector> {

    public MotionDetected(ObservableMotionDetector source) {
        super(source);
    }

}
