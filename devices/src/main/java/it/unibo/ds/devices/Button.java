package it.unibo.ds.devices;

public interface Button {

    boolean isPressed();

}
