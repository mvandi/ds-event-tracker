package it.unibo.ds.devices.pi4j;

import com.pi4j.io.gpio.*;
import it.unibo.ds.devices.Light;


public class Led implements Light {

    private GpioPinDigitalOutput pin;

    public Led(int pinNum) {
        GpioController gpio = GpioFactory.getInstance();
        pin = gpio.provisionDigitalOutputPin(Config.getPin(pinNum));
    }

    @Override
    public synchronized void switchOn() {
        pin.high();
    }

    @Override
    public synchronized void switchOff() {
        pin.low();
    }

}
