package it.unibo.ds.devices.emu;

import it.unibo.ds.devices.Light;

public class Led implements Light {

    private final String id;

    public Led(int pinNum, String id) {
        this.id = id;
    }

    public Led(int pinNum) {
        this(pinNum, "green");
    }

    @Override
    public synchronized void switchOn() {
        System.out.println("[LED " + id + "] ON");
    }

    @Override
    public synchronized void switchOff() {
        System.out.println("[LED " + id + "] OFF");
    }

}
