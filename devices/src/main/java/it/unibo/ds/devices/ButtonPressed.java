package it.unibo.ds.devices;

import it.unibo.ds.events.Event;

public class ButtonPressed extends Event<ObservableButton> {

    public ButtonPressed(ObservableButton source) {
        super(source);
    }

}
