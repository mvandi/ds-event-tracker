package it.unibo.ds.devices;

import it.unibo.ds.events.Event;

public class ObjDetected extends Event<ObservableProximitySensor> {

    private final double distance;

    public ObjDetected(ObservableProximitySensor source, double distance) {
        super(source);
        this.distance = distance;
    }

    public double getDistance() {
        return distance;
    }
}
