package it.unibo.ds.devices;

public interface Actuator {
    void startMovement();

    void stopMovement();
}
