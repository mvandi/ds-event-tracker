package it.unibo.ds.devices.pi4j;

import com.pi4j.io.gpio.*;

public final class GpioBox {

    private static final Pin[] pinsDesc = {
            RaspiPin.GPIO_00, RaspiPin.GPIO_01, RaspiPin.GPIO_02, RaspiPin.GPIO_03,
            RaspiPin.GPIO_04, RaspiPin.GPIO_05, RaspiPin.GPIO_06, RaspiPin.GPIO_07,
            RaspiPin.GPIO_08, RaspiPin.GPIO_09, RaspiPin.GPIO_10, RaspiPin.GPIO_11,
            RaspiPin.GPIO_12, RaspiPin.GPIO_13, RaspiPin.GPIO_14, RaspiPin.GPIO_15,
            RaspiPin.GPIO_16, RaspiPin.GPIO_17, RaspiPin.GPIO_18, RaspiPin.GPIO_19,
            RaspiPin.GPIO_20};

    private static GpioPinDigitalInput[] pins;

    public static void printState() {
        for (int i = 0; i < pins.length; i++) {
            System.out.print("" + i + ":" + pins[i].getState().getValue() + " ");
        }
        System.out.println(".");
    }

    static {
        GpioController gpio = GpioFactory.getInstance();
        pins = new GpioPinDigitalInput[pinsDesc.length];
        for (int i = 0; i < pinsDesc.length; i++) {
            pins[i] = gpio.provisionDigitalInputPin(pinsDesc[i]);
        }
    }

    private GpioBox() {
    }

}
