package it.unibo.ds.devices.pi4j;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinPullResistance;
import it.unibo.ds.devices.MotionDetector;

public class Pir implements MotionDetector {

    private GpioPinDigitalInput pin;

    public Pir(int pinNum) {
        GpioController gpio = GpioFactory.getInstance();
        pin = gpio.provisionDigitalInputPin(Config.getPin(pinNum), PinPullResistance.PULL_DOWN);
    }

    @Override
    public boolean detected() {
        return pin.isHigh();
    }

}
