package it.unibo.ds.devices.pi4j;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.GpioUtil;
import it.unibo.ds.devices.TemperatureSensor;

public class DHT11 implements TemperatureSensor {

    private static final int MAX_TIMINGS = 85;

    private final int[] data = {0, 0, 0, 0, 0};
    private final int pin;

    public DHT11(final int pinNum) {
        pin = pinNum;

        // setup wiringPi
        if (Gpio.wiringPiSetup() == -1) {
            throw new IllegalStateException("GPIO SETUP FAILED");
        }

        GpioUtil.export(pin, GpioUtil.DIRECTION_OUT);
    }

    @Override
    public double getTemperature() {
        int lastState = Gpio.HIGH;
        int j = 0;
        data[0] = data[1] = data[2] = data[3] = data[4] = 0;

        Gpio.pinMode(pin, Gpio.OUTPUT);
        Gpio.digitalWrite(pin, Gpio.LOW);
        Gpio.delay(18);

        Gpio.digitalWrite(pin, Gpio.HIGH);
        Gpio.pinMode(pin, Gpio.INPUT);

        for (int i = 0; i < MAX_TIMINGS; i++) {
            int counter = 0;
            while (Gpio.digitalRead(pin) == lastState) {
                counter++;
                Gpio.delayMicroseconds(1);
                if (counter == 255) {
                    break;
                }
            }

            lastState = Gpio.digitalRead(pin);

            if (counter == 255) {
                break;
            }

            /* ignore first 3 transitions */
            if (i >= 4 && i % 2 == 0) {
                /* shove each bit into the storage bytes */
                data[j / 8] <<= 1;
                if (counter > 16) {
                    data[j / 8] |= 1;
                }
                j++;
            }
        }
        // check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
        if (j >= 40 && checkParity()) {
            double temperature = (double) (((data[2] & 0x7F) << 8) + data[3]) / 10;
            if (temperature > 125) {
                temperature = data[2]; // for DHT11
            }
            if ((data[2] & 0x80) != 0) {
                temperature = -temperature;
            }
            return temperature;
        } else {
            throw new IllegalArgumentException("Data not good, skip");
        }
    }

    private boolean checkParity() {
        return data[4] == (data[0] + data[1] + data[2] + data[3] & 0xFF);
    }

}
