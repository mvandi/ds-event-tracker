package it.unibo.ds.devices;

public interface Light {
    void switchOn();

    void switchOff();
}
