package it.unibo.ds.devices;

import it.unibo.ds.events.EventSource;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ObservableTimer extends EventSource<Tick> {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> tickHandle;
    private final Runnable tickTask;

    public ObservableTimer() {
        tickTask = () -> notifyEvent(new Tick(this, System.currentTimeMillis()));
    }

    /**
     * Start generating tick event
     *
     * @param period period in milliseconds
     */
    public synchronized void start(long period) {
        tickHandle = scheduler.scheduleAtFixedRate(tickTask, 0, period, TimeUnit.MILLISECONDS);
    }

    /**
     * Generate a tick event after a number of milliseconds
     *
     * @param deltat schedule delta in milliseconds
     */
    public synchronized void scheduleTick(long deltat) {
        scheduler.schedule(tickTask, deltat, TimeUnit.MILLISECONDS);
    }

    /**
     * Stop generating tick event
     */
    public synchronized void stop() {
        if (tickHandle != null) {
            tickHandle.cancel(false);
            tickHandle = null;
        }
    }

}
