package it.unibo.ds.devices;

import it.unibo.ds.events.Event;

public class ButtonReleased extends Event<ObservableButton> {

    public ButtonReleased(ObservableButton source) {
        super(source);
    }

}
