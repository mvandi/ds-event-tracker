package it.unibo.ds.devices;

public interface ProximitySensor {

    boolean isObjDetected();

    double getObjDistance();

}
