package it.unibo.ds.devices;

public interface MotionDetector {
    boolean detected();
}
