package it.unibo.ds.messages;

public class StartAlarm extends Communication {

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof StartAlarm && super.equals(o));
    }

}
