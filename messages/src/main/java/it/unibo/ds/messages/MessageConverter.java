package it.unibo.ds.messages;

import io.vertx.core.json.JsonObject;

import java.util.Objects;

public final class MessageConverter {

    private static final String START_ALARM = "startAlarm";
    private static final String IGNORE_ALARM = "ignoreAlarm";
    private static final String STOP_ALARM = "stopAlarm";
    private static final String PRESENCE = "presence";
    private static final String TEMPERATURE = "temperature";
    public static final String TYPE_KEY = "type";

    public static JsonObject toJson(final Object message) {
        final JsonObject json = JsonObject.mapFrom(message);
        if (message instanceof StartAlarm) {
            json.put(TYPE_KEY, START_ALARM);
        } else if (message instanceof IgnoreAlarm) {
            json.put(TYPE_KEY, IGNORE_ALARM);
        } else if (message instanceof StopAlarm) {
            json.put(TYPE_KEY, STOP_ALARM);
        } else if (message instanceof Presence) {
            json.put(TYPE_KEY, PRESENCE);
        } else if (message instanceof Temperature) {
            json.put(TYPE_KEY, TEMPERATURE);
        } else {
            throw new IllegalStateException("Invalid message: " + message.getClass().getName());
        }
        return json;
    }

    public static <T> T fromJson(final JsonObject message) {
        if (message.containsKey(TYPE_KEY)) {
            Class<T> cls;
            if (isStartAlarm(message)) {
                cls = (Class<T>) StartAlarm.class;
            } else if (isIgnoreAlarm(message)) {
                cls = (Class<T>) IgnoreAlarm.class;
            } else if (isStopAlarm(message)) {
                cls = (Class<T>) StopAlarm.class;
            } else if (isPresence(message)) {
                cls = (Class<T>) Presence.class;
            } else if (isTemperature(message)) {
                cls = (Class<T>) Temperature.class;
            } else {
                throw new IllegalStateException("Invalid type: " + message.getString(TYPE_KEY));
            }
            message.remove(TYPE_KEY);
            return message.mapTo(cls);
        }
        throw new IllegalStateException("JSON object does not contain key \"" + TYPE_KEY + "\"");
    }

    public static boolean isStartAlarm(final JsonObject json) {
        return isType(json, START_ALARM);
    }

    public static boolean isIgnoreAlarm(final JsonObject json) {
        return isType(json, IGNORE_ALARM);
    }

    public static boolean isStopAlarm(final JsonObject json) {
        return isType(json, STOP_ALARM);
    }

    public static boolean isPresence(final JsonObject json) {
        return isType(json, PRESENCE);
    }

    public static boolean isTemperature(final JsonObject json) {
        return isType(json, TEMPERATURE);
    }

    public static boolean isCommunication(final JsonObject json) {
        return isStartAlarm(json) || isIgnoreAlarm(json) || isStopAlarm(json) || isPresence(json) || isTemperature(json);
    }

    private static boolean isType(final JsonObject json, final String type) {
        return json != null && json.containsKey(TYPE_KEY) && Objects.equals(json.getString(TYPE_KEY), type);
    }

    private MessageConverter() {
    }

}
