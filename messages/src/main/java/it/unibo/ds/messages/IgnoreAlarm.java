package it.unibo.ds.messages;

public class IgnoreAlarm extends Communication {

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof IgnoreAlarm && super.equals(o));
    }

}
