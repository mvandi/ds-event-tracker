package it.unibo.ds.messages;

import java.util.Objects;

public class Temperature extends Communication {

    private double value;

    public Temperature() {
    }

    public Temperature(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof Temperature && super.equals(o)
                && Double.compare(((Temperature) o).getValue(), getValue()) == 0);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue());
    }

    private void setValue(final double value) {
        this.value = value;
    }

}
