package it.unibo.ds.messages;

public class StopAlarm extends Communication {

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof StopAlarm && super.equals(o));
    }

}
