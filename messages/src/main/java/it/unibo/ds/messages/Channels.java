package it.unibo.ds.messages;

public final class Channels {

    private static final String PREFIX = "events";

    private static final String ANDROID_EVENTS = PREFIX + ".android";

    public static final String ANDROID_EVENTS_SERVER = ANDROID_EVENTS + ".server";
    public static final String ANDROID_EVENTS_CLIENT = ANDROID_EVENTS + ".client";

    public static final String EVENTS = PREFIX;
    public static final String WEB_EVENTS = PREFIX + ".web";

    private Channels() {
    }

}
