package it.unibo.ds.messages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public abstract class Communication {

    private static final SimpleDateFormat utcFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());
    private String timestamp;

    protected Communication() {
        setTimestamp(new Date());
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Communication)) return false;
        return Objects.equals(getTimestamp(), ((Communication) o).getTimestamp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTimestamp());
    }

    private void setTimestamp(final Date timestamp) {
        setTimestamp(utcFormat.format(timestamp));
    }

    private void setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
    }

}
