package it.unibo.ds.messages;

public class Presence extends Communication {

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof Presence && super.equals(o));
    }

}
