@echo off

set ALARM_FLAG=-a
set BUTTON_FLAG=-b
set LIGHT_FLAG=-l
set EMU_FLAG=-e

set argc=0
FOR %%x in (%*) DO (
    set /A argc+=1
    set "argv[!argc!]=%%~x"
)

IF /I "%argc%" LSS "1" (
    goto usage
)
set PACKAGE=it.unibo.ds.alarm_manager
IF /I "%1" == "%ALARM_FLAG%" (
    set mainClass=%PACKAGE%.AlarmManager
) ELSE if /I "%1" == "%BUTTON_FLAG%" (
    set mainClass=%PACKAGE%.AlarmButtonManager
) ELSE if /I "%1" == "%LIGHT_FLAG%" (
    set mainClass=%PACKAGE%.AlarmLightManager
) ELSE (
    echo "Wrong flag %1" >&2
    goto usage
)
set OUTPUT_DIR=build\libs
IF /I "%1" == "%ALARM_FLAG%" (
    java -cp %OUTPUT_DIR%/alarm-manager-1.0-SNAPSHOT.jar %mainClass%
) ELSE (
    if /I "%argc%" LSS "2" (
        goto usage
    )
    IF /I "%2" == "%EMU_FLAG%" (
        if /I "%argc%" == "3" (
            set emu="true"
            set pin=%3
        ) ELSE (
            goto usage
        )
    ) ELSE (
        IF /I "%argc%" == "2" (
            set emu="false"
            set pin=%2
        ) ELSE (
            goto usage
        )
    )
    java -cp %OUTPUT_DIR%/alarm-manager-1.0-SNAPSHOT.jar %mainClass% %emu% %pin%
)
goto exit_program

:usage
@echo "usage: %0 <%ALARM_FLAG%|%BUTTON_FLAG%|%LIGHT_FLAG%> [%EMU_FLAG%] <pin>"
@echo " %ALARM_FLAG% start the alarm verticle"
@echo " %BUTTON_FLAG% start the button verticle"
@echo " %LIGHT_FLAG% start the light verticle"
@echo " %EMU_FLAG% start in emulated mode"

:exit_program
