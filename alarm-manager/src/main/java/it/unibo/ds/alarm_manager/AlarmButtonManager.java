package it.unibo.ds.alarm_manager;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import it.unibo.ds.devices.ButtonPressed;
import it.unibo.ds.devices.ObservableButton;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.messages.StopAlarm;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.IntFunction;

import static it.unibo.ds.messages.Channels.EVENTS;

public class AlarmButtonManager extends RxVerticleBase {

    private enum State {
        NO_ALARM,
        ALARM
    }

    private final AtomicReference<State> state = new AtomicReference<>(State.NO_ALARM);
    private ObservableButton alarmButton;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        final boolean emulated = config().getBoolean("emulated");
        final int pin = config().getInteger("pin");
        alarmButton = getButtonFactory(emulated).apply(pin);
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isStartAlarm)
                .subscribe(this::onStartAlarm);

        alarmButton.toObservable()
                .filter(e -> isAlarm() && e instanceof ButtonPressed)
                .cast(ButtonPressed.class)
                .subscribe(this::onButtonPressed);

        logger.info("Started");
    }

    private void onStartAlarm(final JsonObject message) {
        alarm();
        logger.info("Alarm started");
    }

    private void onButtonPressed(final ButtonPressed event) {
        noAlarm();
        eventBus.publish(EVENTS, MessageConverter.toJson(new StopAlarm()));
        logger.info("Alarm button pressed");
    }

    private void alarm() {
        state.set(State.ALARM);
    }

    private void noAlarm() {
        state.set(State.NO_ALARM);
    }

    private boolean isAlarm() {
        return state.get() == State.ALARM;
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("emulated", Boolean.parseBoolean(args[0]))
                        .put("pin", Integer.parseInt(args[1]))))
                .deploy(AlarmButtonManager::new);
    }

    private static IntFunction<? extends ObservableButton> getButtonFactory(final boolean emulated) {
        return emulated
                ?  it.unibo.ds.devices.emu.Button::new
                :  it.unibo.ds.devices.pi4j.Button::new;
    }

}
