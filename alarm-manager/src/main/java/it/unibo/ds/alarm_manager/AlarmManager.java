package it.unibo.ds.alarm_manager;

import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import io.vertx.reactivex.core.eventbus.MessageConsumer;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.messages.StartAlarm;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import static it.unibo.ds.messages.Channels.EVENTS;

public class AlarmManager extends RxVerticleBase {

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isPresence)
                .subscribe(this::onPresence);

        logger.info("Started");
    }

    private void onPresence(final JsonObject message) {
        final MessageConsumer<JsonObject> consumer = eventBus.consumer(EVENTS);
        final long tid = vertx.setTimer(10_000, ignore -> {
            eventBus.publish(EVENTS, MessageConverter.toJson(new StartAlarm()));
            logger.info("Timeout elapsed started alarm");
            if (consumer.isRegistered())
                consumer.unregister();
        });

        consumer.toObservable()
                .map(Message::body)
                .filter(m -> MessageConverter.isStartAlarm(m) || MessageConverter.isIgnoreAlarm(m))
                .subscribe(m -> {
                    vertx.cancelTimer(tid);
                    logger.info("User responded to detected presence");
                    if (consumer.isRegistered()) {
                        consumer.unregister();
                    }
                });
        logger.info("Detected presence, started timeout of {0,number,#} seconds", 10);
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploy(AlarmManager::new);
    }

}
