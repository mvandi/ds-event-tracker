#!/bin/bash
ALARM_FLAG=-a
BUTTON_FLAG=-b
LIGHT_FLAG=-l
EMU_FLAG=-e

usage() {
    echo -e "usage: $0 <$ALARM_FLAG|$BUTTON_FLAG|$LIGHT_FLAG> [$EMU_FLAG] <pin>"
    echo -e "\t$ALARM_FLAG start the alarm verticle"
    echo -e "\t$BUTTON_FLAG start the button verticle"
    echo -e "\t$LIGHT_FLAG start the light verticle"
    echo -e "\t$EMU_FLAG start in emulated mode"
}
if [ "$#" -lt "1" ]; then
    usage
    exit 1
fi

PACKAGE=it.unibo.ds.alarm_manager
if [ "$1" == "$ALARM_FLAG" ]; then
    mainClass=$PACKAGE.AlarmManager
elif [ "$1" == "$BUTTON_FLAG" ]; then
    mainClass=$PACKAGE.AlarmButtonManager
elif [ "$1" == "$LIGHT_FLAG" ]; then
    mainClass=$PACKAGE.AlarmLightManager
else
    echo "Wrong flag $1" >&2
    exit 1
fi
OUTPUT_DIR=build/libs
if [[ "$1" == "$LIGHT_FLAG" || "$1" == "$BUTTON_FLAG" ]]; then
    if [ "$#" -lt "2" ]; then
        usage
        exit 1
    fi
    if [[ "$2" -ne "$EMU_FLAG" ]]; then
        emu="false"
        pin=$2
    else
        if [ "$#" -ne "3" ]; then
            usage
            exit 1
        fi
        emu="true"
        pin=$3
    fi
    java -cp $OUTPUT_DIR/alarm-manager-1.0-SNAPSHOT.jar $mainClass $emu $pin
else
    java -cp $OUTPUT_DIR/alarm-manager-1.0-SNAPSHOT.jar $mainClass
fi
