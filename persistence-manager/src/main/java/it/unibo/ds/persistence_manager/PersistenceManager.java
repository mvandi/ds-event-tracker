package it.unibo.ds.persistence_manager;

import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.Message;
import io.vertx.reactivex.ext.mongo.MongoClient;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.spi.FileSystemProvider;
import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;

import static it.unibo.ds.messages.Channels.EVENTS;

public class PersistenceManager extends RxVerticleBase {

    private MongoClient client;
    private String collectionName;

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        final JsonObject config = config();
        client = MongoClient.createShared(this.vertx, config);
        collectionName = config.getString("collection_name");
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(MessageConverter::isCommunication)
                .subscribe(this::onMessage);

        logger.info("Started");
    }

    private void onMessage(final JsonObject message) {
        client.rxInsert(collectionName, message)
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onSuccess(final String objectId) {
                        logger.info("Persisted: {0}", message);
                    }

                    @Override
                    public void onError(final Throwable cause) {
                        logger.error("Could not persist message: {0}", cause.getMessage());
                    }
                });
    }

    public static void main(final String... args) throws IOException, URISyntaxException {
        final String content = Files.readAllLines(getPath("/mlab_config.json"))
                .stream().collect(Collectors.joining());

        final JsonObject config = new JsonObject(content);

        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(config))
                .deploy(PersistenceManager::new);
    }

    private static Path getPath(final String path) throws URISyntaxException, IOException {
        final URI uri = PersistenceManager.class.getResource(path).toURI();

        if (uri.getScheme().equalsIgnoreCase("jar")) {
            for (final FileSystemProvider provider : FileSystemProvider.installedProviders()) {
                if (provider.getScheme().equalsIgnoreCase("jar")) {
                    try {
                        provider.getFileSystem(uri);
                    } catch (final FileSystemNotFoundException e) {
                        // in this case we need to initialize it first
                        provider.newFileSystem(uri, Collections.emptyMap());
                    }
                }
            }
        }
        return Paths.get(uri);
    }

}
