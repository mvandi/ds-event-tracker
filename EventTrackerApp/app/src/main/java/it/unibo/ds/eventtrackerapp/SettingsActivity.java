package it.unibo.ds.eventtrackerapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import static it.unibo.ds.eventtrackerapp.C.EXTRA_ADDRESS;
import static it.unibo.ds.eventtrackerapp.C.EXTRA_MESSAGE;
import static it.unibo.ds.eventtrackerapp.C.EXTRA_PORT;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = "SettingsActivity";

    private EditText mAddress;
    private EditText mPort;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_MESSAGE)) {
            Toast.makeText(this, intent.getStringExtra(EXTRA_MESSAGE), Toast.LENGTH_LONG).show();
        }

        mAddress = findViewById(R.id.address);
        mPort = findViewById(R.id.port);
        findViewById(R.id.connect).setOnClickListener(this::onClick);
    }

    private void onClick(final View ignore) {
        final String address = getAddress();
        final String port = getPort();
        Log.d(TAG, "Address: " + address);
        Log.d(TAG, "Port: " + port);

        String message = "";
        boolean error = false;
        if (!Validations.isAddress(address)) {
            message += address + " is not a valid mAddress!";
            error = true;
        }
        if (!Validations.isPort(port)) {
            if (error)
                message += "\n";
            message += port + " is not a valid mPort!";
            error = true;
        }
        if (error) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            return;
        }

        final Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(EXTRA_ADDRESS, address);
        intent.putExtra(EXTRA_PORT, Integer.parseInt(port));
        startActivity(intent);
    }

    @NonNull
    private String getAddress() {
        return mAddress.getText().toString();
    }

    @NonNull
    private String getPort() {
        return mPort.getText().toString();
    }

}
