package it.unibo.ds.eventtrackerapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ItemListViewAdapter extends BaseAdapter {

    private final LayoutInflater mInflater;

    private final Context mContext;
    private final List<String> mData;

    public ItemListViewAdapter(Context context, List<String> data) {
        this.mContext = context;
        this.mData = data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View vi = view;
        if (vi == null)
            vi = mInflater.inflate(R.layout.row, null);
        TextView text = vi.findViewById(R.id.text_row_items);
        TextView title = vi.findViewById(R.id.title_row_items);
        if (mData.get(i).indexOf("confermato") > 0) {
            title.setText("Start Alarm");
        } else {
            title.setText("Ignore Alarm");
        }
        text.setText(mData.get(i));
        return vi;
    }

}
