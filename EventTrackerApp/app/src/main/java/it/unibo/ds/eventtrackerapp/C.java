package it.unibo.ds.eventtrackerapp;

final class C {

    private static final String EXTRA_PREFIX = C.class.getPackage().getName() + ".extras";

    static final String EXTRA_MESSAGE = EXTRA_PREFIX + ".message";
    static final String EXTRA_ADDRESS = EXTRA_PREFIX + ".address";
    static final String EXTRA_PORT = EXTRA_PREFIX + ".port";

    static final String EVENTS_ANDROID_CLIENT = "events.android.client";
    static final String EVENTS_ANDROID_SERVER = "events.android.server";

    static final String TYPE_START_ALARM = "startAlarm";
    static final String TYPE_IGNORE_ALARM = "ignoreAlarm";
    static final String TYPE_PRESENCE = "presence";

    private C() {
    }

}
