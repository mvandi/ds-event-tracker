package it.unibo.ds.eventtrackerapp;

import java.util.regex.Pattern;

final class Validations {

    private static final Pattern sAddressPattern;
    private static final Pattern sPortPattern;

    static {
        final String addressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
        sAddressPattern = Pattern.compile(addressRegex);
        final String portRegex = "[0-9]|[1-5]?[0-9]{1,4}|6((5(5(3[0-5]|[0-2][0-9])|[0-4][0-9]{1,2}))|[0-4][0-9]{1,3})";
        sPortPattern = Pattern.compile(portRegex);
    }

    static boolean isAddress(final String s) {
        return sAddressPattern.matcher(s).matches();
    }

    static boolean isPort(final String s) {
        return sPortPattern.matcher(s).matches();
    }

    private Validations() {
    }

}
