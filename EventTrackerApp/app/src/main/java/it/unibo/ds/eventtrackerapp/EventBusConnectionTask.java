package it.unibo.ds.eventtrackerapp;

import android.os.AsyncTask;

import ng.abdlquadri.eventbus.EventBus;
import ng.abdlquadri.eventbus.handlers.ConnectHandler;

import java.util.concurrent.CountDownLatch;

abstract class EventBusConnectionTask extends AsyncTask<Void, Void, Boolean> {

    private final String mAddress;
    private final int mPort;

    protected EventBusConnectionTask(final String address, final int port) {
        mAddress = address;
        mPort = port;
    }

    @Override
    protected final Boolean doInBackground(final Void... ignore) {
        final boolean[] connected = {false};
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        EventBus.connect(mAddress, mPort, new ConnectHandler() {
            @Override
            public void onConnect(final boolean isConnected) {
                connected[0] = isConnected;
                countDownLatch.countDown();
            }

            @Override
            public void onDisConnect(final Throwable throwable) {
                onDisconnect(throwable);
            }
        });
        try {
            countDownLatch.await();
        } catch (final InterruptedException e) {
            throw new RuntimeException(e);
        }
        return connected[0];
    }

    @Override
    protected final void onProgressUpdate(final Void... ignore) {
    }

    protected final void onPostExecute(final Boolean connected) {
        if (connected != null && connected) {
            onConnected();
        } else {
            onNotConnected();
        }
    }

    protected abstract void onConnected();

    protected abstract void onNotConnected();

    protected abstract void onDisconnect(Throwable cause);

}
