package it.unibo.ds.eventtrackerapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ng.abdlquadri.eventbus.EventBus;

import static it.unibo.ds.eventtrackerapp.C.*;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final DateFormat sUtcFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault());

    private AlertDialog mDialog;

    private final List<String> mListItems = new ArrayList<>();
    private BaseAdapter mAdapter;
    private String mAddress;
    private int mPort;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView settings = findViewById(R.id.text_settings);
        final ListView listView = findViewById(R.id.list_item);

        final Intent intent = getIntent();
        mAddress = intent.getStringExtra(EXTRA_ADDRESS);
        mPort = intent.getIntExtra(EXTRA_PORT, -1);

        settings.setText("Address: " + mAddress + "\tPort: " + mPort);

        makeToast("address: " + mAddress + ", [ort: " + mPort, Toast.LENGTH_LONG).show();

        mAdapter = new ItemListViewAdapter(this, mListItems);
        listView.setAdapter(mAdapter);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(R.string.dialog_presence_title)
                .setMessage(R.string.dialog_presence_message)
                .setPositiveButton(R.string.confirm_alarm, (dialogInterface, i) -> {
                    addItems(TYPE_START_ALARM);
                    EventBus.publish(EVENTS_ANDROID_CLIENT, newMessage(TYPE_START_ALARM));
                })
                .setNegativeButton(R.string.ignore_alarm, (dialogInterface, i) -> {
                    addItems(TYPE_IGNORE_ALARM);
                    EventBus.publish(EVENTS_ANDROID_CLIENT, newMessage(TYPE_IGNORE_ALARM));
                });

        this.mDialog = builder.create();
    }

    @Override
    protected void onStart() {
        super.onStart();
        new ConnectionTask(mAddress, mPort).execute();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.close();
    }

    private void addItems(final String type) {
        switch (type) {
            case TYPE_START_ALARM:
                this.mListItems.add("Hai confermato un Allarme in mData: " + sUtcFormat.format(new Date()));
                break;
            case TYPE_IGNORE_ALARM:
                this.mListItems.add("Hai ignorato un Allarme in mData: " + sUtcFormat.format(new Date()));
                break;
        }
        mAdapter.notifyDataSetChanged();
    }

    private String newMessage(final String type) {
        try {
            return new JSONObject().put("type", type).put("timestamp", sUtcFormat.format(new Date())).toString();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeDialog() {
        mDialog.cancel();
    }

    private void showDialog() {
        mDialog.show();
    }

    private Toast makeToast(final CharSequence text, final int duration) {
        return Toast.makeText(this, text, duration);
    }

    private final class ConnectionTask extends EventBusConnectionTask {
        ConnectionTask(final String address, final int port) {
            super(address, port);
        }

        @Override
        protected void onConnected() {
            EventBus.registerHandler(EVENTS_ANDROID_SERVER, m -> {
                try {
                    final JSONObject message = new JSONObject(m).getJSONObject("body");
                    if (message.has("type")) {
                        Log.d(TAG, "EventBus handler executed on " + Thread.currentThread().getName());
                        Log.d(TAG, "Activity: " + MainActivity.this.toString());
                        final String type = message.getString("type");
                        switch (type) {
                            case TYPE_PRESENCE:
                                runOnUiThread(MainActivity.this::showDialog);
                                break;
                            case TYPE_START_ALARM:
                                runOnUiThread(MainActivity.this::closeDialog);
                                break;
                            default:
                                throw new IllegalStateException("Type not understood: " + type);
                        }
                    }
                } catch (final JSONException e) {
                    throw new RuntimeException(e);
                }
            });
            makeToast("Connected!", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onNotConnected() {
            final Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            intent.putExtra(EXTRA_MESSAGE, "Could not connect to " + mAddress + " at port" + mPort);
            startActivity(intent);
        }

        @Override
        protected void onDisconnect(final Throwable cause) {
            makeToast("Disconnected from EventBus: " + cause.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }
    }

}
