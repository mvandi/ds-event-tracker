package it.unibo.ds.communication_bridge;

import io.vertx.core.Context;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.BridgeOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.eventbus.bridge.tcp.TcpEventBusBridge;
import io.vertx.reactivex.core.eventbus.Message;
import it.unibo.ds.messages.MessageConverter;
import it.unibo.ds.vertx.RxVerticleBase;
import it.unibo.ds.vertx.deploy.Deployer;

import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicReference;

import static it.unibo.ds.messages.Channels.ANDROID_EVENTS_SERVER;
import static it.unibo.ds.messages.Channels.ANDROID_EVENTS_CLIENT;
import static it.unibo.ds.messages.Channels.EVENTS;


public class TcpCommunicationBridge extends RxVerticleBase {

    private int port;

    private enum State {
        IDLE,
        SENT_NOTIFICATION
    }

    private final AtomicReference<State> state = new AtomicReference<>(State.IDLE);

    @Override
    public void init(final Vertx vertx, final Context context) {
        super.init(vertx, context);
        port = config().getInteger("port");
    }

    @Override
    public void start() throws Exception {
        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(m -> isIdle() && MessageConverter.isPresence(m))
                .subscribe(this::onPresence);

        eventBus.<JsonObject>consumer(EVENTS).toObservable()
                .map(Message::body)
                .filter(m -> isSentNotification() && MessageConverter.isStartAlarm(m))
                .subscribe(this::onStartAlarm);

        eventBus.<JsonObject>consumer(ANDROID_EVENTS_CLIENT).toObservable()
                .map(Message::body)
                .filter(m -> isSentNotification() && (MessageConverter.isStartAlarm(m) || MessageConverter.isIgnoreAlarm(m)))
                .subscribe(this::onResponse);

        final TcpEventBusBridge bridge = tcpEventBusBridge();
        final String address = InetAddress.getLocalHost().getHostAddress();
        bridge.listen(port, address, ar -> {
            if (ar.succeeded()) {
                logger.info("TCP Event Bus Bridge started on http://{0}:{1,number,#}", address, port);
            } else {
                logger.error("Could not start TCP Event Bus Bridge: {0}", ar.cause().getMessage());
            }
        });
    }

    private void onPresence(final JsonObject message) {
        sentNotification();
        eventBus.publish(ANDROID_EVENTS_SERVER, message);
        logger.info("Sent presence detected notification to Android");
    }

    private void onStartAlarm(final JsonObject message) {
        idle();
        eventBus.publish(ANDROID_EVENTS_SERVER, message);
        logger.info("Sent alarm started notification to Android");
    }

    private void onResponse(final JsonObject message) {
        idle();
        eventBus.publish(EVENTS, message);
        logger.info("Received response from Android");
    }

    private boolean isIdle() {
        return state.get() == State.IDLE;
    }

    private boolean isSentNotification() {
        return state.get() == State.SENT_NOTIFICATION;
    }

    private void idle() {
        state.set(State.IDLE);
    }

    private void sentNotification() {
        state.set(State.SENT_NOTIFICATION);
    }

    private TcpEventBusBridge tcpEventBusBridge() {
        return TcpEventBusBridge.create(vertx.getDelegate(), new BridgeOptions()
                .addInboundPermitted(new PermittedOptions().setAddress(ANDROID_EVENTS_CLIENT))
                .addOutboundPermitted(new PermittedOptions().setAddress(ANDROID_EVENTS_SERVER)));
    }

    public static void main(final String... args) {
        Deployer.deployer()
                .clustered(true)
                .deploymentOptions(new DeploymentOptions().setConfig(new JsonObject()
                        .put("port", Integer.parseInt(args[0]))))
                .deploy(TcpCommunicationBridge::new);
    }

}
