@echo off

set argc=0
FOR %%x in (%*) DO (
    set /A argc+=1
    set "argv[!argc!]=%%~x"
)

IF /I "%argc%" == "1" (
    java -cp build\libs\communication-bridge-1.0-SNAPSHOT.jar it.unibo.ds.communication_bridge.TcpCommunicationBridge %1
) ELSE (
    goto usage
)
goto exit_program

:usage
@echo "usage: %0 <port>"

:exit_program
