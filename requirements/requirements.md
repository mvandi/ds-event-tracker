# Event Tracker

# Requisiti del sistema

Si vuole realizzare un sistema che permetta di fare il tracciamento nel tempo dei valori acquisiti con sensori e la gestione di situazioni di allarme come segue.

Il sistema deve essere composto da:

 - sottosistema S1 su Raspberry Pi, che funge da centralina
    - Led verde L1
    - Led verde L2
    - Led rosso L3
    - Pulsante tattile T1
 - sottosistema S2 su Rasperry Pi, con trasduttori
    - sensore temperatura (analogico o digitale)
    - rilevatore di movimento PIR
    - dispositivo di attuazione a scelta A
 - sottosistema S3 su Android
 - sottosistema S4 su portatile

I sottosistemi comunicano utilizzando un bus di eventi distribuito come canale di comunicazione condiviso.

# Funzionalità del sistema

Il sistema deve:

 - Tracciare l'andamento della temperatura considerando campionamenti con un certo periodo P e gli eventi significativi relativi alle presenze rilevate (alle ore XX rilevata presenza, alle ore YY segnalato allarme - per l'allarme vedere punto in seguito).
 - Quando viene rilevata la presenza di qualcuno, sul sistema S3 deve essere richiesto se notificare o meno una situazione di allarme. In caso affermativo, deve essere acceso L3 e attivato il dispositivo di attuazione A. L'allarme può essere disattivato solo premendo il pulsante tattile T1, che riporta il sistema al normale funzionamento. La conferma o meno deve arrivare entro 10 secondi, passati i quali viene considerata una situazione di allarme.
 - Il led L1 deve essere acceso ad indicare che il sistema è in funzione. Mentre il led L2 deve lampeggiare ogni volta che avviene una comunicazione fra S1 e S2.
 - Rendere accessibili/consultabili i dati rilevati via web, installando su S4 un server web.
